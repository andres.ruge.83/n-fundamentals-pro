import { Injectable } from '@nestjs/common';
import { CreateSongDto } from './dto/create-song.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Song } from './songs.entity';
import { In, Repository, UpdateResult } from 'typeorm';
import { UpdateSongDto } from './dto/update-song.dto';
import {
  IPaginationOptions,
  Pagination,
  paginate,
} from 'nestjs-typeorm-paginate';
import { Artist } from 'src/artists/artists.entity';

@Injectable()
export class SongsService {
  constructor(
    @InjectRepository(Song)
    private songRepostory: Repository<Song>,
    @InjectRepository(Artist)
    private artistRepository: Repository<Artist>,
  ) {}
  private readonly songs = [];

  async create(songDTO: CreateSongDto): Promise<Song> {
    const song = new Song();

    song.title = songDTO.title;
    // song.artists = songDTO.artists;
    song.duration = songDTO.duration;
    song.releasedDate = songDTO.releasedDate;
    song.lyrics = songDTO.lyrics;

    const artists = await this.artistRepository.findBy({
      id: In(songDTO.artists),
    });
    // const artists = await this.artistRepository.findByIds(songDTO.artists);
    song.artists = artists;

    return await this.songRepostory.save(song);
  }

  findAll(): Promise<Song[]> {
    return this.songRepostory.find();
  }

  findOne(id: number): Promise<Song> {
    return this.songRepostory.findOneBy({ id });
  }

  async delete(id: number): Promise<void> {
    await this.songRepostory.delete(id);
  }

  update(id: number, recordToUpdate: UpdateSongDto): Promise<UpdateResult> {
    return this.songRepostory.update(id, recordToUpdate);
  }

  async paginate(options: IPaginationOptions): Promise<Pagination<Song>> {
    const queryBuilder = this.songRepostory.createQueryBuilder('c');
    queryBuilder.orderBy('c.releasedDate', 'DESC');
    return paginate<Song>(queryBuilder, options);
  }
}
