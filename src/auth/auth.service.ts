import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { LoginDto } from './dto/login.dto';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { ArtistsService } from 'src/artists/artists.service';
import { PayloadType } from 'src/types/payload.type';
import { Enable2FAType } from 'src/types/auth.type';
import * as speakeasy from 'speakeasy';
import { UpdateResult } from 'typeorm';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private artistService: ArtistsService,
    private configService: ConfigService,
  ) {}

  async login(
    loginDto: LoginDto,
  ): Promise<
    { access_token: string } | { validate2FA: string; message: string }
  > {
    try {
      const user = await this.userService.findOne(loginDto);

      const passwordMatched = await bcrypt.compare(
        loginDto.password,
        user.password,
      );
      if (!passwordMatched) {
        throw new UnauthorizedException('bad credentials');
      }

      if (!user.enable2FA) {
        const payload: PayloadType = { email: user.email, userId: user.id };

        const artist = await this.artistService.findArtist(user.id);
        if (artist) {
          payload.artistId = artist.id;
        }
        return {
          access_token: this.jwtService.sign(payload),
        };
      } else {
        return {
          validate2FA: `http://localhost:3000/auth/validate-2fa?userId=${user.id}`,
          message:
            'Please send the one-time password/token from your Google Authenticator App',
        };
      }
    } catch (error) {
      throw new UnauthorizedException('bad credentials');
    }
  }

  async enable2FA(userId: number): Promise<Enable2FAType> {
    const user = await this.userService.findById(userId);
    if (user.enable2FA) {
      return { secret: user.twoFASecret };
    }
    const secret = speakeasy.generateSecret();
    console.log(secret);
    user.twoFASecret = secret.base32;
    this.userService.updateSecretKey(user.id, user.twoFASecret);
    return { secret: user.twoFASecret };
  }

  async disable2FA(userId: number): Promise<UpdateResult> {
    return this.userService.disable2FA(userId);
  }

  async validate2FAToken(
    userId: number,
    token: string,
  ): Promise<{ access_token: string }> {
    try {
      const user = await this.userService.findById(userId);
      if (!user || user.enable2FA === false) {
        throw new UnauthorizedException('Invalid user for 2FA validation');
      }
      const verified = speakeasy.totp.verify({
        secret: user.twoFASecret,
        token,
        encoding: 'base32',
      });

      if (verified) {
        const payload: PayloadType = { email: user.email, userId: user.id };
        return {
          access_token: this.jwtService.sign(payload),
        };
      } else {
        throw new UnauthorizedException('Invalid 2FA token');
      }
    } catch (error) {
      throw new UnauthorizedException('Error verifying 2FA token');
    }
  }

  getEnvVariables() {
    return this.configService.get<number>('password');
  }
}
