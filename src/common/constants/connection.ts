export const connection = {
  CONNECTION_STRING: 'CONNNECTION_STRING',
  DB: 'MYSQL',
  DBNAME: 'TEST',
};

export type Connection = {
  CONNECTION_STRING: string;
  DB: string;
  DBNAME: string;
};
